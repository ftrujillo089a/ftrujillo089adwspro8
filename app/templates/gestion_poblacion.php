<?php
    error_reporting(E_ALL ^ E_NOTICE);
 // Datos constantes.
  //  include 'config.php';
 //   include_once("Poblacion.php"); //clase Poblacion
 //   include_once("alta_instalacion.php");       
 //   include_once("control_funciones.php");
//    sesion(); // función que comprueba si se ha introducido login
    
    
?>
<html>
    <head>
        <title>Poblaciones Españolas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto1.css">
    </head>
    <body>    
        
     <h1 id='titulo'><?=Config::$titulo?></h1>
     <div>GESTIÓN de POBLACIONES</div><br>
    
    <h4>Listado:</h4>
    <table> 
       <tr>
	<th>Código</th>
        <th>Código Provincia</th>
	<th>Nombre</th>
	<th>Superficie</th>
	<th>Habitantes</th>
        <th>Gobierno</th>

    </tr>
    <?php 
    
       foreach ($parametro['poblaciones'] as $poblacion) : ?>
        <tr>
            <td><?php echo $poblacion->getCodigo(); ?></td>
            <td><?php echo $poblacion->getCodigo_Provincia(); ?></td>
            <td><?php echo $poblacion->getNominacion(); ?></td>
            <td><?php echo $poblacion->getSuperficie(); ?></td>
            <td><?php echo $poblacion->getHabitantes(); ?></td>
            <td><?php echo $poblacion->getGobierno(); ?></td>
        </tr>
    <?php endforeach; ?>
      </table>
    <a  id='inicio' href="../web/index.php?control=principal">Inicio</a>
    <a id='opciones' href="../web/index.php?control=altaPoblacion">Altas</a>
    <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>     
    </body>
</html>
