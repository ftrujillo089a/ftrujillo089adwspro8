<?php
    error_reporting(E_ALL ^ E_NOTICE);
 // Datos constantes.
 //   include 'config.php';
   // include_once("../app/modelo/Provincia.php"); 
    
     
?>
<html>
    <head>
        <title>Provincias Españolas</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/proyecto1.css">
    </head>
    <body>    
        
     <h1 id='titulo'><?=Config::$titulo?></h1>
     <div>GESTIÓN de PROVINCIAS</div><br>
    
    <h>Listado:</h>
    <table> 
       <tr>
	<th>Código</th>
	<th>Nombre</th>
	<th>Superficie</th>
	<th>Habitantes</th>
        <th>Comunidad</th>

    </tr>
  
    <?php 
    
       foreach ($parametro['provincias'] as $provincia) : ?>
        <tr>
            <td><?php echo $provincia->getCodigo(); ?></td>
            <td><?php echo $provincia->getNominacion(); ?></td>
            <td><?php echo $provincia->getSuperficie(); ?></td>
            <td><?php echo $provincia->getHabitantes(); ?></td>
            <td><?php echo $provincia->getComunidad(); ?></td>
        </tr>
    <?php endforeach; ?>
       </table>
    <a  id='inicio' href="../web/index.php?control=principal">Inicio</a>
    <a href="../web/index.php?control=altaProvincia">Altas</a><br>
    <div id="pie"><?=Config::$autor?> <?=Config::$fecha?> <?=Config::$empresa?> <?=Config::$curso?></div>     
    </body>
</html>
