<?php

/* clase Controller, maneja las vistas y los modelos de datos.
 */
class Controller {
 /* Si el valor login='no' no comprueba el login en la base de datos.
  * Esto se hace por si no exite la BBDD y se quiere instalar */   
    public function  login() {
      $parametro=array();   
     if (Config::$login == 'yes') {  //  comprobar login
        
   // Se crea un objeto modelo con el atributo conexion y error.
     $modelo=tipoModelo();
     //Comprobamos si existe la base de datos.En el atributo error del modelo,
     // existe el mensaje
     if  ( !$modelo->error ==''){
      $parametro['mensaje'][0]='No existe la BBDD.';
      $parametro['mensaje'][1]='Contacte con su administrador para que le de instrucciones.';
      $parametro['mensaje'][2]='Las instrucciones son: cambiar valor en config.php de la variable login=no.';
      $parametro['mensaje'][3]='Y para acceder admin/admin';
        
      require __DIR__ .'/templates/form_error.php';  // Mostrar error
     exit;   
     };
     }
     //   $modelo=new ModeloMySql(Config::$bdhostname,  Config::$bdnombre,  Config::$bdusuario,  Config::$bdclave);

    if ($_SERVER['REQUEST_METHOD'] == 'POST' ) {   
     
      if(isset($_REQUEST['usuario'])) {
        $usuar = htmlspecialchars(trim(strip_tags($_REQUEST['usuario'])));
        $clave = htmlspecialchars(trim(strip_tags($_REQUEST['password'])));
        
        if (  Config::$login == 'yes') {  //sólo comprobar en ete caso       
                // leer usuario para saber si existe
         $obj_usuario=new Usuario($usuar, $clave);
        // $mensaje=$modelo->login($obj_usuario);
         $parametro=array('mensaje'=> $modelo->login($obj_usuario));
      }
    // Si tiene valor el elemento 0, es que hay mensaje de error, si no, continuar.   
         if ($parametro['mensaje'][0]=="") { // existe, continuar  
         $_SESSION['usuario'] = $_POST['usuario'];
         require __DIR__ .'/templates/menu_principal.php';
         exit;                    
        }else {
            require __DIR__ .'/templates/form_error.php';  // Mostrar error
        exit;
        }  //}
        
    }else {
     if (isset($_SESSION['usuario'])) { 
     require __DIR__ .'/templates/menu_principal.php';  
     exit;
    }}
        
       } // aún no han introducido login 
       if (isset($_SESSION['usuario'])) { 
     require __DIR__ .'/templates/menu_principal.php';  
     exit;
    }
       require __DIR__ .'/templates/login.php';
    }
//-----------------------------------------------------------------------------    
    public function  principal() {
        require __DIR__ .'/templates/menu_principal.php';
    }
    
//-----------------------------------------------------------------------------
    public function  mostrarProvincias() {
    $this->sesion(); // función que comprueba si se ha introducido login
      // Crear objeto modelo y crear array con los objetos provincia, 
      // para pasarla al formulario.
    $modelo=tipoModelo();
    // $modelo=new ModeloMySql(Config::$bdhostname,  Config::$bdnombre,  Config::$bdusuario,  Config::$bdclave); 
     $parametro = array('provincias' => $modelo->mostrarProvincias(),);
        require __DIR__ .'/templates/gestion_provincia.php';
    } 
//-----------------------------------------------------------------------------
    
    public function  altaProvincia() {
    $this->sesion(); // función que comprueba si se ha introducido login 
     $modelo=tipoModelo();   
    // $modelo=new ModeloMySql(Config::$bdhostname,  Config::$bdnombre,  Config::$bdusuario,  Config::$bdclave); 
     if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
     $obj_provincia=new Provincia($_REQUEST['codigo'],$_REQUEST['nominacion'],$_REQUEST['superficie'],
             $_REQUEST['habitantes'],$_REQUEST['comunidad']);    
      $modelo->altaProvincia($obj_provincia);   
      header('location:index.php?control=altaProvincia');
     }
     
       require __DIR__ .'/templates/form_provincia.php';
    }    
//-----------------------------------------------------------------------------
    public function  mostrarPoblaciones() {
     $this-> sesion(); // función que comprueba si se ha introducido login
     $modelo=tipoModelo();
    // $modelo=new ModeloMySql(Config::$bdhostname,  Config::$bdnombre,  Config::$bdusuario,  Config::$bdclave); 
     $parametro = array('poblaciones' => $modelo->mostrarPoblaciones(),);
       
        require __DIR__ .'/templates/gestion_poblacion.php';
    } 
 //-----------------------------------------------------------------------------
    public function  altaPoblacion() {
    $this->sesion(); // función que comprueba si se ha introducido login 
    $modelo=tipoModelo();
    // $modelo=new ModeloMySql(Config::$bdhostname,  Config::$bdnombre,  Config::$bdusuario,  Config::$bdclave); 
     if ($_SERVER['REQUEST_METHOD'] == 'POST') { 
     $obj_poblacion=new Poblacion($_REQUEST['codigo'],$_REQUEST['codigo_provincia'],$_REQUEST['nominacion'],
             $_REQUEST['superficie'],$_REQUEST['habitantes'],$_REQUEST['gobierno']);    
      $modelo->altaPoblacion($obj_poblacion);  
      
      header('location:index.php?control=altaPoblacion');
     } else {
        // Obtener los código de las provincias para combo
      $parametro = array('codigosProvincia' => $modelo->codigosProvincia(),);
     
     }
     
       require __DIR__ .'/templates/form_poblacion.php';
    } 
//-----------------------------------------------------------------------------
    public function sesion() {
       // session_start();
        if (!isset($_SESSION['usuario'])) {
        header("Location: index.php"); 
          }
    }
 //-----------------------------------------------------------------------------
    public function cerrarSesion(){
      session_start();
      session_destroy();  // destruir la sesión
      header("location:index.php?control=login"); // mostrar pantalla login  
    }
//-----------------------------------------------------------------------------
    public function altaInstalacion(){
    $this-> sesion(); // función que comprueba si se ha introducido login
  //  $modelo=tipoModelo();
    $modelo=new ModeloMySql(Config::$bdhostname, "",  Config::$bdusuario,  Config::$bdclave);
    $baseDatos =  Config::$bdnombre;
    $tabla ='provincia';
    $tabla2='poblacion';
    $clavePrimaria ='codigo';
    $claveAjena='';
    $obj_instalacion= new Instalacion($baseDatos, $tabla, $tabla2, $clavePrimaria, $claveAjena);
    
    $parametro = array('instalacion' => $modelo->instalacion($obj_instalacion)); 
    require __DIR__ .'/templates/gestion_instalacion.php';  
   
    }
    
    } // fin clase
