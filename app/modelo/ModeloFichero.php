<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 */

class ModeloFichero implements IModelo{
    
    public function __construct() {
        
    }
 
  //--------------------------------------------------------------------------   
  public function login($obj_usuario) {
      $mensaje = array();   
     if ( $obj_usuario->getUsuario()== '' OR $obj_usuario->getClave() ==''){
      //return FALSE;  
       
         $mensaje[0]='Error, debes introducir un usuario/contraseña correctos.';
         $mensaje[1]='Para altas ponerse en contacto con el administrador.';
     }else {
     //return TRUE;
         $mensaje[0]='';
     }  
      return $mensaje;
    }
 //--------------------------------------------------------------------------
     public function altaPoblacion($obj_poblacion) {
        $file = fopen($_SERVER['DOCUMENT_ROOT'] .Config::$rutaTxt .'poblaciones.txt', "a");
            $linea = $obj_poblacion->getCodigo() . ";" . $obj_poblacion->getCodigo_provincia() . ";" .
                     $obj_poblacion->getNominacion() .  ";" . $obj_poblacion->getSuperficie() . ";" . 
                     $obj_poblacion->getHabitantes() . ";" . $obj_poblacion->getGobierno() . "\r\n";
            fwrite($file, $linea);           
            fclose($file);
    }
     //--------------------------------------------------------------------------
    public function mostrarPoblaciones() {
       $poblaciones = array();
       $file = fopen($_SERVER['DOCUMENT_ROOT'] .Config::$rutaTxt .'poblaciones.txt', "r");
       $indice=0;     
            while (!feof($file)){
           $linea= fgets($file) ;
        //  dividir en variables (separación ;). Cada elemento del array tiene un dato
         
           $array_datos=  explode(';', $linea);
             
           $obj_poblacion=new Poblacion($array_datos[0], $array_datos[1], $array_datos[2],
                   $array_datos[3], $array_datos[4], $array_datos[5]);
            $poblaciones[$indice]=$obj_poblacion;
             $indice++;                                              
     
          }
            fclose($file);   
            return $poblaciones;
    }
    //--------------------------------------------------------------------------
    public function altaProvincia($obj_provincia) {
      $file = fopen($_SERVER['DOCUMENT_ROOT'] .Config::$rutaTxt .'provincias.txt', "a");
            $linea = $obj_provincia->getCodigo() . ";" . $obj_provincia->getNominacion() .  ";" .
                    $obj_provincia->getSuperficie() . ";" . $obj_provincia->getHabitantes() . ";" .
                    $obj_provincia->getComunidad() . "\r\n";
            fwrite($file, $linea);           
            fclose($file);  
    }
    //--------------------------------------------------------------------------
    public function mostrarProvincias() {
       $provincias = array();
       $file = fopen($_SERVER['DOCUMENT_ROOT'] .Config::$rutaTxt .'provincias.txt', "r");
        $indice=0;  
            while (!feof($file)){
              
                $linea= fgets($file) ;
        //  dividir en variables (separación ;). Cada elemento del array tiene un dato
         
           $array_datos=  explode(';', $linea);
        
           $obj_provincia=new Provincia($array_datos[0], $array_datos[1], $array_datos[2],
                   $array_datos[3], $array_datos[4]);
       
          $provincias[$indice]=$obj_provincia;
          $indice++;
          }
            fclose($file); 
            return $provincias;
    }
    //--------------------------------------------------------------------------
     public function codigosProvincia() {
    $file = fopen($_SERVER['DOCUMENT_ROOT'] .Config::$rutaTxt  .'provincias.txt', "r");
        $indice=0;
         $codigosProvincia = array();    
            while (!feof($file)){
           $linea= fgets($file) ;
        //  dividir en variables (separación ;). Cada elemento del array tiene un dato
           $array_datos=  explode(';', $linea);      
           $obj_provincia=new Provincia($array_datos[0], $array_datos[1],"","","");
           $codigosProvincia[$indice]=$obj_provincia;
           $indice++;
                  }
            fclose ($file);
            return $codigosProvincia;
     }
     
}
