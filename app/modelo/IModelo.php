<?php

/* 
 * Interface para fichero y MySql
 */

interface IModelo {
public function login($obj_usuario);   
public function mostrarProvincias(); 
public function altaProvincia($obj_provincia); 
public function mostrarPoblaciones(); 
public function altaPoblacion($obj_poblacion); 
public function codigosProvincia() ;

}
?>