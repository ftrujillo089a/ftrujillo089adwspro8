<?php

/* 
 clase Poblacion
 */
class Poblacion {
   // Atributos
    private $codigo;
    private $codigo_provincia;
    private $nominacion;
    private $superficie;
    private $habitantes;
    private $gobierno;
    
// constructor    
    public function __construct($codigo,$codigo_provincia,$nominacion,$superficie,$habitantes,$gobierno) {
        $this -> codigo = $codigo;
        $this -> codigo_provincia = $codigo_provincia;
        $this -> nominacion = $nominacion;
        $this -> superficie = $superficie;
        $this -> habitantes = $habitantes;
        $this -> gobierno = $gobierno;        
    } 
    
    // Métodos getter
    function getCodigo() {
        return $this->codigo;
    }

    function getCodigo_provincia() {
        return $this->codigo_provincia;
    }

    function getNominacion() {
        return $this->nominacion;
    }

    function getSuperficie() {
        return $this->superficie;
    }

    function getHabitantes() {
        return $this->habitantes;
    }

    function getGobierno() {
        return $this->gobierno;
    }
  // Métodos setter
    function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    function setCodigo_provincia($codigo_provincia) {
        $this->codigo_provincia = $codigo_provincia;
    }

    function setNominacion($nominacion) {
        $this->nominacion = $nominacion;
    }

    function setSuperficie($superficie) {
        $this->superficie = $superficie;
    }

    function setHabitantes($habitantes) {
        $this->habitantes = $habitantes;
    }

    function setGobierno($gobierno) {
        $this->gobierno = $gobierno;
    }

}