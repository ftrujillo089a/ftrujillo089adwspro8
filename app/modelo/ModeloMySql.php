<?php
error_reporting(E_ALL ^ E_NOTICE);
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once 'IModelo.php';
include_once 'Usuario.php';
include_once 'Provincia.php';
include_once 'Poblacion.php';
include_once 'Instalacion.php';
 
class ModeloMySql implements IModelo{
    protected $conexion;
    public $error;
    public function __construct($bdHostName,$baseDatos,$userName,$passWd) {
  // Si no paso base de datos, porque no existe, y quiero crearla. 
       if ($baseDatos == ''){
              $dsn='mysql:host='.$bdHostName ;  
            }else {
              $dsn='mysql:host='.$bdHostName .';dbname='.$baseDatos;  
           }
          
     try {
             $mySQL =new PDO($dsn, $userName, $passWd) ;   
             $mySQL->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY,TRUE);
         
         $this->conexion=$mySQL;
        } catch (PDOException $e) {
           //  echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
             
           $this->error='Falló la conexión: ' . $e->getMessage(). "<br>";

        }
    }
//--------------------------------------------------------------------------   
    public function login($obj_usuario) {
       // Control login
         $contador=0;
         $mensaje = array(); 
         $mensaje='';
        $consulta='SELECT * FROM USUARIO where usuario="'.$obj_usuario->getUsuario()
                 .'" AND clave="'.$obj_usuario->getClave() .'"';   
              
         $resultado=  $this->conexion->query($consulta);
         $contador =  $resultado->rowCount(); // nº registros
         
         if ($contador <= 0) { 
          $mensaje[0]='Error, debes introducir un usuario/contraseña correctos.';
          $mensaje[1]='Para altas ponerse en contacto con el administrador.';
         }  
         return $mensaje;
              
    }
    //--------------------------------------------------------------------------
    public function mostrarProvincias() {
       $consulta='SELECT * FROM PROVINCIA';
         $indice=0;
         $provincias = array();
        
          try {
          $datos=$this->conexion->query($consulta);
          if ($datos) {
           foreach ($datos as $registro) {
                                              
            // guardar en array
             $obj_provincia=new Provincia($registro[codigo],$registro[nominacion],
             $registro[superficie], $registro[habitantes], $registro[comunidad]);
             $provincias[$indice]=$obj_provincia;
             $indice++;
               
              }
              }
          $conexion=NULL;  //cerrar
          return $provincias;
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        }    
    }
    //--------------------------------------------------------------------------
     public function altaProvincia($obj_provincia) {
         $consulta='INSERT INTO PROVINCIA (codigo,nominacion,superficie,habitantes,comunidad) 
              VALUES (' .$obj_provincia->getCodigo() .',"'.$obj_provincia->getNominacion().'",'
                  .$obj_provincia->getSuperficie().','.$obj_provincia->getHabitantes() .',"'
                  .$obj_provincia->getComunidad() .'")';
     try {
          $this->conexion->query($consulta); 
          $conexion=NULL;  //cerrar 
      } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        }    
            
        }
    //--------------------------------------------------------------------------    
    public function mostrarPoblaciones() {
          
         $consulta='SELECT * FROM POBLACION';
         $indice=0;
         $poblaciones = array();
        
          try {
          $datos=$this->conexion->query($consulta);
          if ($datos) {
           foreach ($datos as $registro) {
                            
           $obj_poblacion=new Poblacion($registro[codigo], $registro[codigo_provincia], 
           $registro[nominacion],$registro[superficie], $registro[habitantes], $registro[gobierno]);
           $poblaciones[$indice]=$obj_poblacion;
           $indice++; 
      
              }
              }
          $conexion=NULL;  //cerrar
          return $poblaciones;
        } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        } 
               
    }  
    //----Guardar en array las provincias para altas de poblaciones-------------
        public function codigosProvincia() {
            $consulta='SELECT * FROM PROVINCIA';	
          $indice=0;
         $codigosProvincia = array();         
         $datos=$this->conexion->query($consulta);
         if ($datos) {
         foreach ($datos as $registro) {    
           $obj_provincia=new Provincia($registro[codigo],$registro[nominacion],0,0,'');
           $codigosProvincia[$indice]=$obj_provincia;
           $indice++;
         }        
              }
          $conexion=NULL;  //cerrar
          return $codigosProvincia;
        }  
    //--------------------------------------------------------------------------
      public function altaPoblacion($obj_poblacion) {
         $consulta='INSERT INTO POBLACION (codigo,codigo_provincia, nominacion,superficie,habitantes,gobierno) 
              VALUES (' .$obj_poblacion->getCodigo() .',' .$obj_poblacion->getCodigo_provincia()
                  .',"'.$obj_poblacion->getNominacion().'",'
                  .$obj_poblacion->getSuperficie().','.$obj_poblacion->getHabitantes() .',"'
                  .$obj_poblacion->getGobierno() .'")';
     try {
          $this->conexion->query($consulta); 
          $conexion=NULL;  //cerrar 
      } catch (PDOException $e) {
             echo 'Falló la conexión: ' . $e->getMessage(). "<br>";
        }    
            
        }
   //--------------------------------------------------------------------------     
    public function instalacion($obj_instalacion) {  
        $instalacion = array(); 
      // Obtener datos del objeto para la instalación
      // BBDD, nombre tablas,clave primaria.
     $baseDatos=$obj_instalacion->getBaseDatos();
     $tabla=$obj_instalacion->getTabla();
     $tabla2=$obj_instalacion->getTabla2();
     $claveP=$obj_instalacion->getClavePrimaria();
     // Crear BBDD
     $instalacion[0]=$this->crearBaseDatos($baseDatos); 
     
      $conexion=NULL;
     // cierro la conexión  y la vuelvo a abrir, pero esta vez con base de datos.
     $this->__construct(Config::$bdhostname,  Config::$bdnombre,  Config::$bdusuario,  Config::$bdclave);
     // Crear tablas
     $instalacion[1]=$this->crearTabla($tabla,$claveP);   
     $instalacion[2]=$this->crearTabla($tabla2,$claveP);
          // Clave ajena
     $instalacion[3]=$this->claveAjena();
          // crear tabla usuario/login TEMA 8
     $instalacion[4]=$this->crearTablaUsuario();
     
     $conexion=NULL;
     
     return $instalacion;
    }
    
 //--------------------------------------------------------------------------
    function crearBaseDatos($baseDatos) {
             $consulta='CREATE DATABASE '.Config::$bdnombre;
             
        try {          
            $this->conexion->query($consulta); 
          return 'Creada BBDD : '.Config::$bdnombre. "<br>";
        } catch (PDOException $e) {
           return 'Falló la conexión: ' . $e->getMessage(). "<br>";
        }   
        }
     // Crear tabla
     function crearTabla($tabla, $claveP){
     if ($tabla=='provincia') {
        $consulta='CREATE TABLE '.$tabla .'( 
            codigo INTEGER NOT NULL,
            nominacion VARCHAR(50),
            superficie INTEGER,
            habitantes INTEGER,
            comunidad VARCHAR(50),
     PRIMARY KEY (' .$claveP  .') )'; 
     }
     
     if ($tabla=='poblacion') { 
        $consulta='CREATE TABLE '.$tabla .'( 
            codigo INTEGER NOT NULL,
            codigo_provincia INTEGER NOT NULL,
            nominacion VARCHAR(50),
            superficie INTEGER,
            habitantes INTEGER,
            gobierno VARCHAR(50),
     PRIMARY KEY (' .$claveP  .') )'; 
     }
       try {
              $this->conexion->query($consulta); 
      return 'CREADA LA TABLA: '.$tabla. "<br>";
     
        } catch (PDOException $e) {
             return 'Falló la conexión: ' . $e->getMessage(). "<br>";
        } 
     }
    //--------------------------------------------------------------------------
     function claveAjena(){
         $consulta='ALTER TABLE poblacion 
            ADD CONSTRAINT claveProvincia 
            FOREIGN KEY (codigo_provincia) 
            REFERENCES provincia (codigo) 
            ON DELETE NO ACTION 
            ON UPDATE NO ACTION ';
         try {
              $this->conexion->query($consulta); 
                return 'CREADA CAj: Población -> Provincia'. "<br>";
        } catch (PDOException $e) {
             return 'Falló la conexión: ' . $e->getMessage(). "<br>";
        } 
         }
    //--------------------------------------------------------------------------
        // Crear tabla usuario
         function crearTablaUsuario(){
             
           $consulta='CREATE TABLE usuario( 
            usuario VARCHAR(15) NOT NULL,
            clave  VARCHAR(15) NOT NULL,
            PRIMARY KEY (usuario) )'; 
     
       try {
              $this->conexion->query($consulta); 
        // insertar el usuario admin/admin
              $consulta='INSERT INTO USUARIO (usuario,clave) 
              VALUES ("admin","admin")';
              $this->conexion->query($consulta);
              return 'CREADA LA TABLA: usuario' ."<br>";
        } catch (PDOException $e) {
             return 'Falló la conexión: ' . $e->getMessage(). "<br>";
         
         } }            
    
}