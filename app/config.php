<?php

/* 
 Constantes para pie de página y datos BBDD
 */
  class Config {
    static public $titulo='PROVINCIAS ESPAÑOLAS Y SUS POBLACIONES';
    static public $autor='Paqui Trujillo';
    static public $fecha='2017-01-12';
    static public $empresa='CEEDcv';
    static public $curso='2016-2017';
    static public $tema = "Tema 8. Patrón Modelo-Vista-Controlador (MVC).";
    static public $bdhostname = "localhost";
    static public $bdnombre = "ceedcv";
    static public $bdusuario = "alumno";
    static public $bdclave = "alumno";
    static public $modelo = "mysql";  // para trabajar con ficheros cambiar modelo='fichero'
                                     // con base datos , modelo='mysql'
    static public $login = "yes"; // para no comprobar login, si no existe la BBDD, login='no'
                                 // para comprobar, login='yes'
    static public $rutaTxt="/ftrujillo089adwspro8/app/txt/";
}
?>
