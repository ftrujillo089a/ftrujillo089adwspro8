<?php 
 // iniciar sesión
 session_start();   
// CONTROLADOR FRONTAL
//  En función de los parámetros que lleguen en la query string se determinará
// qué acciones se debe realizar para construir la página solicitada.
       
// Cargar Modelo y  Controlador
     require_once __DIR__ . '/../app/Config.php';
     require_once __DIR__ . '/../app/modelo/ModeloMySql.php';
     require_once __DIR__ . '/../app/modelo/ModeloFichero.php';
     require_once __DIR__ . '/../app/Controller.php';
     require_once __DIR__ .'/../app/utils/control_funciones.php';
     
 // Definir rutas
   $arrayRutas=array('login' => array('controller' => 'Controller', 'action' =>'login'),
                     'principal' => array('controller' => 'Controller', 'action' =>'principal'),
                     'mostrarProvincias' => array('controller' => 'Controller', 'action' =>'mostrarProvincias'),
                     'altaProvincia' => array('controller' => 'Controller', 'action' =>'altaProvincia'),
                     'mostrarPoblaciones' => array('controller' => 'Controller', 'action' =>'mostrarPoblaciones'),
                     'altaPoblacion' => array('controller' => 'Controller', 'action' =>'altaPoblacion'),
                     'cerrarSesion' => array('controller' => 'Controller', 'action' =>'cerrarSesion'),
                     'altaInstalacion' => array('controller' => 'Controller', 'action' =>'altaInstalacion') );
   // Obtener ruta
  
  $ruta=$_GET['control']; //variable de la URL
  if (isset($ruta)) {  //tiene valor
      if ($arrayRutas[$ruta]){   // ¿ está definida la ruta en el array ?
            
  }  else {
      // error
      exit;
  }            
  }else{   // no tiene valor la ruta
   $ruta='login' ;// por omisión, si no hay valor en el GET, va a pantalla que pide login
  }
  $controlador=$arrayRutas[$ruta];
//   echo $controlador['controller'].'->'.$controlador['action'];
    // Llamar a la página seleccionada 
   if (method_exists($controlador['controller'],$controlador['action'])) {
       call_user_func(array(new $controlador['controller'],$controlador['action']));
  
      }
   
        ?>
    